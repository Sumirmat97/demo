<%@ page import = "JDBC.*" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Log In!</title>
	</head>
	
	<body>
		<% 
		
		String username = request.getParameter("_username");
		String pass = Query.hashMD5(request.getParameter("_password"));
		
		if(Query.login(username,pass) == true)
		{
			out.println("Welcome");
			%>
			 <br/> 
			<% 
			out.println("Username:" + username );
			%> 
			<br/> 
			<% 
			if(Query.showId(username)!= "-1")
			{
				out.println("Id: "+Query.showId(username));
			}
		}
		else if(Query.login(username,pass) == false)
		{
			out.println("Wrong credentials entered");
		}
		%>
		<br/><br/><br/>
		<a href="index.html">Go to Homepage</a> 	
		
	</body>
</html>