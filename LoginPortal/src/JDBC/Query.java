package JDBC;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Query {
	public static Connection conn;
	
	public static Connection getConnection() throws SQLException {
	
		//do not create a connection if it does exists
		if (conn == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "sumir");
		}
		return conn;
	}
	
	//method to register new students 
	public static int reg(String Id,String _username,String pass){
		
		
		try {
			ResultSet rs;
		
			conn=getConnection();
			Statement sta = conn.createStatement();
			rs = sta.executeQuery("Select * from public.register(\'"+Id+"\',\'"+_username+"\',\'"+pass+"\');");
			rs.next();
			
			rs.close();
			return 1;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return 0;
		
		}
	}
	
	//method to verify credentials 
	public static boolean login(String username,String password){
		try {
			ResultSet rs;
			conn = getConnection();
			Statement sta = conn.createStatement();
			rs = sta.executeQuery("Select * from public.login(\'"+username+"\',\'"+password+"\');");
			rs.next();
			if(rs.getBoolean(1))
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return false;
		
		
	}
	
	//method to verify credentials 
	public static String showId(String username){
		try {
			ResultSet rs;
			conn = getConnection();
			Statement sta = conn.createStatement();
			rs = sta.executeQuery("Select * from public.getid(\'"+username+"\');");
			rs.next();
			return rs.getString(1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return "-1";
	
	}
	
	public static String hashMD5(String password) 
	{
	
		StringBuffer hashPassword = null;
		try {
			String plainPassword = password;
			MessageDigest mdAlgorithm = MessageDigest.getInstance("MD5");
			mdAlgorithm.update(plainPassword.getBytes());

			byte[] digest = mdAlgorithm.digest();
			hashPassword = new StringBuffer();

			for (int i = 0; i < digest.length; i++) 
			{
			    plainPassword = Integer.toHexString(0xFF & digest[i]);

			    if (plainPassword.length() < 2)
			    {
			        plainPassword = "0" + plainPassword;
			    }

			    hashPassword.append(plainPassword);
			}
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}
			return hashPassword.toString();
			 
		}

}